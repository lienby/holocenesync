#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <psp2/ctrl.h>
#include <psp2/kernel/threadmgr.h>
#include <sys/time.h>
#include <taihen.h>
#include <vitasdk.h>

#include "graphics.h"
#include "rtcUserBridge.h"

#define printf psvDebugScreenPrintf

int ret;
SceUID fd;

int get_key(void) {
	static unsigned buttons[] = {
		SCE_CTRL_SELECT,
		SCE_CTRL_START,
		SCE_CTRL_UP,
		SCE_CTRL_RIGHT,
		SCE_CTRL_DOWN,
		SCE_CTRL_LEFT,
		SCE_CTRL_LTRIGGER,
		SCE_CTRL_RTRIGGER,
		SCE_CTRL_TRIANGLE,
		SCE_CTRL_CIRCLE,
		SCE_CTRL_CROSS,
		SCE_CTRL_SQUARE,
	};

	static unsigned prev = 0;
	SceCtrlData pad;
	while (1) {
		memset(&pad, 0, sizeof(pad));
		sceCtrlPeekBufferPositive(0, &pad, 1);
		unsigned newb = prev ^ (pad.buttons & prev);
		prev = pad.buttons;
		for (int i = 0; i < sizeof(buttons)/sizeof(*buttons); ++i)
			if (newb & buttons[i])
				return buttons[i];

		sceKernelDelayThread(1000); // 1ms
	}
}

int WriteFile(char *file, void *buf, int size) {
	SceUID fd = sceIoOpen(file, SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 0777);
	if (fd < 0)
		return fd;

	int written = sceIoWrite(fd, buf, size);

	sceIoClose(fd);
	return written;
}

int ReadFile(char *file, void *buf, int size) {
	SceUID fd = sceIoOpen(file,SCE_O_RDONLY, 0777);
	if (fd < 0)
		return fd;

	int read = sceIoRead(fd, buf, size);

	sceIoClose(fd);
	return read;
}

int getFileSize(const char *file) {
	SceUID fd = sceIoOpen(file, SCE_O_RDONLY, 0);
	if (fd < 0)
		return fd;
	int fileSize = sceIoLseek(fd, 0, SCE_SEEK_END);
	sceIoClose(fd);
	return fileSize;
}

int CopyFile(char *src, char *dst)
{
	int size = getFileSize(src);
	if (size <= -1){
		printf("getFileSize Failed, ret: 0x%x\n",size);
	}
	else
	{
	char *buffer = malloc(size);
	ret = ReadFile(src,buffer,size);
	if (ret <= -1)
		{
			printf("ReadFile failed 0x%x\n",ret);
		}
		
	ret = WriteFile(dst,buffer,size);
	if (ret <= -1){
			printf("WriteFile failed 0x%x\n",ret);
		}
	}
	return ret;
}


void load_modules()
{
		
	char kplugin_path[0x200];
	memset(kplugin_path,0x00,0x200);
	
	char uplugin_path[0x200];
	memset(uplugin_path,0x00,0x200);
	
	char titleid[12];
	sceAppMgrUmount("app0:");
	
	
	sceAppMgrAppParamGetString(0, 12, titleid , 256);
	sprintf(kplugin_path, "ux0:app/%s/kern_clockset.skprx", titleid);
	int kernel_modid = 0;
	kernel_modid = taiLoadStartKernelModule(kplugin_path, 0, NULL, 0);
	
	
	if(kernel_modid <= 0 && kernel_modid != 0x8002d013 && kernel_modid != 0x8002d004)
	{
		printf("Error cannot load kern_clockset.skprx error 0x%x\n",kernel_modid);
		get_key();
		sceKernelExitProcess(0);
	}
	
	sceAppMgrAppParamGetString(0, 12, titleid , 256);
	sprintf(uplugin_path, "ux0:app/%s/user_clockset.suprx", titleid);
	int user_modid = 0;
	user_modid = sceKernelLoadStartModule(uplugin_path, 0, NULL, 0, NULL, NULL);
	
	
	if(user_modid <= 0)
	{
		printf("Error cannot load user_clockset.suprx error 0x%x\n",user_modid);
		get_key();
		sceKernelExitProcess(0);
	}
	
	return;
}

void main() {

	psvDebugScreenInit();
	
	load_modules();

	
	SceRtcTick curTime = {0};
	sceRtcGetCurrentTick(&curTime);
	unsigned long long int timestamp = curTime.tick;
	printf("Oldtime: %llu\n",timestamp);
	
	timestamp += 0x46120ce75900000ull;
	
	printf("Newtime: %llu\n",timestamp);
	
	unsigned int split1 = timestamp >> 32;
	unsigned int split2 = timestamp & 0xffffffff;
	
	silRtcSetCurrentTick(split2,split1);
	silRtcSetCurrentNetworkTick(split2,split1);
	silRtcSetCurrentSecureTick(split2,split1);
	
	printf("Completed!\nBlessed be!\n");
	
	get_key();
}
