int ksilRtcSetCurrentTick(unsigned int timestamp1, unsigned int timestamp2);
int ksilRtcSetCurrentNetworkTick(unsigned int timestamp1, unsigned int timestamp2);
int ksilRtcSetCurrentSecureTick(unsigned int timestamp1, unsigned int timestamp2);
int ksilSblPostSsMgrSetCpRtc(unsigned int timestamp);
int ksilDumpNvsAct(unsigned int fileno);
int ksilRestoreNvsAct(unsigned int fileno);